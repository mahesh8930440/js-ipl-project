function matchesWonPerTeamPerYear(allMatches) {
  const allMatchesWonPerTeamPerSeasons = allMatches.reduce(
    (matchesWonPerTeamPerSeason, matches) => {
      let season = matches.season;
      let winner = matches.winner;

      if (
        matchesWonPerTeamPerSeason[season] &&
        matchesWonPerTeamPerSeason[season][winner]
      ) {
        matchesWonPerTeamPerSeason[season][winner]++;
      } else if (
        matchesWonPerTeamPerSeason[season] &&
        !matchesWonPerTeamPerSeason[season][winner]
      ) {
        matchesWonPerTeamPerSeason[season][winner] = 1;
      } else {
        matchesWonPerTeamPerSeason[season] = {};
        matchesWonPerTeamPerSeason[season][winner] = 1;
      }

      return matchesWonPerTeamPerSeason;
    },
    {},
  );

  return allMatchesWonPerTeamPerSeasons;
}

module.exports = matchesWonPerTeamPerYear;
