function extraRunsConcededPerTeam(allDeliveries, allMatches) {
  const matchIdsBelongingTo2016 = new Set();
  const extraRunsPerTeam = {};

  for (let match of allMatches) {
    if (match.season == 2016) {
      matchIdsBelongingTo2016.add(match.id);
    }
  }

  for (let delivery of allDeliveries) {
    let deliveriesId = delivery.match_id;

    if (matchIdsBelongingTo2016.has(deliveriesId)) {
      let bowlingTeam = delivery.bowling_team;
      let extraRuns = delivery.extra_runs;

      if (!extraRunsPerTeam[bowlingTeam]) {
        extraRunsPerTeam[bowlingTeam] = parseInt(extraRuns);
      } else {
        extraRunsPerTeam[bowlingTeam] += parseInt(extraRuns);
      }
    }
  }
  return extraRunsPerTeam;
}

module.exports = extraRunsConcededPerTeam;
