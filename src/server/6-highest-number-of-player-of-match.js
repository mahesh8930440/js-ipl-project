function mostNumberOfPlayerOfMatches(allMatches) {
  const allPlayerOfSeason = {};

  for (let match of allMatches) {
    let season = match.season;

    if (!allPlayerOfSeason[season]) {
      allPlayerOfSeason[season] = {};
    }
    let playerOfMatch = match.player_of_match;

    if (!allPlayerOfSeason[season][playerOfMatch]) {
      allPlayerOfSeason[season][playerOfMatch] = 1;
    } else {
      allPlayerOfSeason[season][playerOfMatch] += 1;
    }
  }

  const highestNumberOfPlayerOfMatches = {};
  for (let season in allPlayerOfSeason) {
    const playerOfMatch = allPlayerOfSeason[season];
    let count = 0;
    let playerName = null;
    for (let player in playerOfMatch) {
      const numberPlayerOfMatches = playerOfMatch[player];

      if (numberPlayerOfMatches > count) {
        playerName = [player];
        count = numberPlayerOfMatches;
      } else if (numberPlayerOfMatches == count) {
        playerName.push(player);
        count = numberPlayerOfMatches;
      }
    }
    highestNumberOfPlayerOfMatches[season] = playerName;
  }
  return highestNumberOfPlayerOfMatches;
}

module.exports = mostNumberOfPlayerOfMatches;
