function strikeRateOfBatsman(allDeliveries, allMatches, batsman) {
  const seasonAndMatchIdData = {};

  for (let match of allMatches) {
    let season = match.season;

    if (!seasonAndMatchIdData[season]) {
      seasonAndMatchIdData[season] = [];
    }
    let matchId = match.id;
    seasonAndMatchIdData[season].push(matchId);
  }
  let batsmanStrikeRates = 0;
  const allbatsmanStrikeRates = {};

  for (let season in seasonAndMatchIdData) {
    let matchesId = seasonAndMatchIdData[season];
    batsmanStrikeRates = 0;

    let { allBall, totalBatsmanrun } = allDeliveries.reduce(
      (totalBallAndBatsmanrun, delivery) => {
        const deliveryMatchId = delivery.match_id;

        if (
          matchesId.includes(deliveryMatchId) &&
          delivery.wide_runs == 0 &&
          delivery.batsman == batsman
        ) {
          totalBallAndBatsmanrun.allBall += 1;
          totalBallAndBatsmanrun.totalBatsmanrun += parseInt(delivery.batsman_runs);
        }
        return totalBallAndBatsmanrun;
      },
      { allBall: 0, totalBatsmanrun: 0 },
    );

    if (allBall > 0) {
      batsmanStrikeRates = ((totalBatsmanrun / allBall) * 100).toFixed(2);
    } else {
      batsmanStrikeRates = 0;
    }

    allbatsmanStrikeRates[season] = batsmanStrikeRates;
  }
  return allbatsmanStrikeRates;
}

module.exports = strikeRateOfBatsman;
