function matchesPlayedForYear(allMatches) {
  const allMatchesPlayedForYear = {};

  for (let match of allMatches) {
    let season = match.season;

    if (allMatchesPlayedForYear[season]) {
      allMatchesPlayedForYear[season] += 1;
    } else {
      allMatchesPlayedForYear[season] = 1;
    }
  }
  return allMatchesPlayedForYear;
}

module.exports = matchesPlayedForYear;
