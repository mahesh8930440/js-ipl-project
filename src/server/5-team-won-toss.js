function teamWinTossAndWinMatch(allMatches) {
  const teamWonTossAndWonMatch = {};

  for (let match of allMatches) {
    let tossWinner = match.toss_winner;
    let matchWinner = match.winner;

    if (tossWinner == matchWinner) {
      if (!teamWonTossAndWonMatch[tossWinner]) {
        teamWonTossAndWonMatch[tossWinner] = 1;
      } else {
        teamWonTossAndWonMatch[tossWinner] += 1;
      }
    }
  }
  return teamWonTossAndWonMatch;
}

module.exports = teamWinTossAndWinMatch;
