function bestBowlerEconomy(allDeliveries, allMatches) {
  const allBowlerData = {};

  for (let delivery of allDeliveries) {
    if (delivery.is_super_over != 0) {
      const runsConceded = parseInt(delivery.total_runs);
      const extras = parseInt(delivery.extra_runs);
      const byeRuns = parseInt(delivery.bye_runs);
      const wideRuns = parseInt(delivery.wide_runs);
      const bowler = delivery.bowler;
      const noBallRuns = parseInt(delivery.noball_runs);

      if (!allBowlerData[bowler]) {
        allBowlerData[bowler] = { runs: runsConceded, balls: 1 };
      } else {
        allBowlerData[bowler].runs += runsConceded;
        allBowlerData[bowler].balls += 1;
      }

      if (byeRuns != 0 || wideRuns != 0) {
        allBowlerData[bowler].balls -= 1;
        allBowlerData[bowler].runs -= byeRuns;
      } else if (noBallRuns != 0) {
        allBowlerData[bowler].balls -= 1;
      }
    }
  }

  for (const bowler in allBowlerData) {
    const economy = (
      (allBowlerData[bowler].runs / allBowlerData[bowler].balls) *
      6
    ).toFixed(2);
    allBowlerData[bowler].economy = economy;
  }

  const allBowlerEconomy = [];

  for (let bowler in allBowlerData) {
    let economy = allBowlerData[bowler].economy;
    allBowlerEconomy.push([bowler, economy]);
  }
  const topBowlerEconomy = allBowlerEconomy.sort(
    (bowler1, bowler2) => parseFloat(bowler1[1]) - parseFloat(bowler2[1]),
  );
  return topBowlerEconomy[0];
}

module.exports = bestBowlerEconomy;
