const mostNumberOfPlayerOfMatches = require('../src/server/6-highest-number-of-player-of-match.js');

test('most-number-of-player-of-matches', () => {
  const testSampleData = [
    {
      id: '1',
      season: '2008',

      player_of_match: 'Yuvraj Singh',
    },
    {
      id: '2',
      season: '2009',

      player_of_match: 'Rashid Khan',
    },
    {
      id: '3',
      season: '2010',

      player_of_match: 'SPD Smith',
    },
    {
      id: '4',
      season: '2009',

      player_of_match: 'Rashid Khan',
    },
    {
      id: '5',
      season: '2011',
      winner: 'SRH',
      player_of_match: 'Rashid Khan',
    },
    {
      id: '6',
      season: '2011',

      player_of_match: 'Rashid Khan',
    },
    {
      id: '7',
      season: '2009',

      player_of_match: 'Rashid Khan',
    },
    {
      id: '8',
      season: '2010',

      player_of_match: 'Ms Dhoni',
    },
    {
      id: '9',
      season: '2013',

      player_of_match: 'SPD Smith',
    },

    {
      id: '11',
      season: '2013',
      player_of_match: 'SPD Smith',
    },
  ];
  const resultData = {
    2008: ['Yuvraj Singh'],
    2009: ['Rashid Khan'],
    2010: ['SPD Smith','Ms Dhoni'],
    2011: ['Rashid Khan'],
    2013: ['SPD Smith'],
  };
  expect(mostNumberOfPlayerOfMatches(testSampleData)).toEqual(resultData);
});
