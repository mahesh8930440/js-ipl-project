const matchesPlayedForYear = require('../src/server/1-matches-per-year');

test('played_matches_per_year', () => {
  const testSampleData = [
    {
      id: '1',
      season: '2008',
    },
    {
      id: '2',
      season: '2010',
    },
    {
      id: '3',
      season: '2011',
    },
    {
      id: '4',
      season: '2010',
    },
    {
      id: '5',
      season: '2008',
    },
    {
      id: '6',
      season: '2011',
    },
    {
      id: '7',
      season: '2009',
    },
    {
      id: '8',
      season: '2013',
    },
    {
      id: '9',
      season: '2013',
    },
    {
      id: '10',
      season: '2010',
    },
  ];
  const resultData = { 2008: 2, 2009: 1, 2010: 3, 2011: 2, 2013: 2 };
  expect(matchesPlayedForYear(testSampleData)).toEqual(resultData);
});
