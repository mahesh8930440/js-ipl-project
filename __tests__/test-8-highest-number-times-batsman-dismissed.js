const  mostNumberOfTimeBatsmanDissimed= require("../src/server/8-highest-number-times-batsman-dismissed.js");

test("most-Number-Of-Time-Batsman-Dissimed", () => {
  const testSampleDataDeliveries = [
    {
      match_id: 1,
      extra_runs: 2,
      bowling_team: "MI",
      bowler: "TS Mills",
      player_dismissed: "DA Warner",
      
    },
    {
      match_id: 2,
      extra_runs: 0,
      bowling_team: "CSK",
      bowler: "Jadeja",
      player_dismissed: "smith",
      
    },
    {
      match_id: 3,
      extra_runs: 1,
      bowling_team: "MI",
      bowler: "TS Mills",
      player_dismissed: "Ms Dhoni",
      
    },
    {
      match_id: 4,
      extra_runs: 3,
      bowling_team: "RR",
      bowler: "A Choudhary",
      player_dismissed: "TS Mills",
      
    },
    {
      match_id: 5,
      extra_runs: 0,
      bowling_team: "CSK",
      bowler: "Jadeja",
      player_dismissed: "smith",
      
    },
  ];
  const resultData = {
    player_dismissed: "smith",
    bowler_name: "Jadeja",
    count: 2,
  };
  expect(mostNumberOfTimeBatsmanDissimed(testSampleDataDeliveries)).toEqual(resultData);
});