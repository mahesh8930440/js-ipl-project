# IPL Project

## Description

- In this project, we will transform the raw data of the Indian Premier League (IPL) to calculate various statistics using JavaScript. The dataset for this project can be downloaded from [Kaggle](https://www.kaggle.com/manasgarg/ipl).

## Data Files

- The project requires two data files:

  1. `deliveries.csv`: Contains information about deliveries in IPL matches.
  2. `matches.csv`: Contains information about IPL matches.

## Tasks and Objectives

- We will implement functions to perform the following tasks and generate JSON output files:

  1. Calculate the number of matches played per year for all the years in IPL.
  2. Determine the number of matches won per team per year in IPL.
  3. Find the extra runs conceded per team in the year 2016.
  4. Identify the top 10 economical bowlers in the year 2015.
  5. Find the number of times each team won the toss and also won the match.
  6. Determine a player who has won the highest number of Player of the Match awards for each season.
  7. Calculate the strike rate of a batsman for each season.
  8. Find the highest number of times one player has been dismissed by another player.
  9. Identify the bowler with the best economy in super overs.

## Project Setup

- Before starting the implementation, make sure to follow these setup instructions:

  1. Create a new repository with the name `js-ipl-data-project` in your Gitlab subgroup.

## Implementation

- Implement separate modules for each of the tasks mentioned above. These modules should be placed in the `src/server/` directory.

- Use the results of these functions to generate JSON files in the `public/output/` directory.

## Checklist

- Before submission, make sure to check the following points:

- Proper and descriptive Git commits.

- The directory structure adheres to the provided format.

- `package.json` contains dependencies and devDependencies as needed for your project.

- A `.gitignore` file is present to exclude unnecessary files from version control.

- Variable names should be intuitive and descriptive.

## Getting Started

### Steps to setup and run the project

    1. Clone the repo from GitHub\*\*

     - git clone https://gitlab.com/mahesh8930440/js-ipl-data-project.git


    2. Install node and npm.


     - sudo apt install nodejs;sudo apt install npm


    3. Navigate to the project directory `jsIplDataProject`.

     - cd jsIplDataProject


    4. Install npm packages

     - This command will install all the dependencies required

     - npm install

    5. Run the command to see the outputs

     - node test


# IPL Static Webserver

## Description

- This repository contains an IPL (Indian Premier League) data analysis project. It includes data processing, visualization, and a simple web app for displaying the data using Highcharts.

## Getting Started

### Setting Up the Web Server

    1. switch to `webapp` branch.

     - git checkout webapp.

    2. Set up a static web server in the `public` directory:

     -cd src/public.

### Running the Web Server

    1. Run the server.

     - npm start

    2. The web app fetches IPL data from the JSON files and displays it using Highcharts.
