const fs = require("fs");
const Papa = require("papaparse");
const {allDeliveries,allMatches}= require("./src/data/allDeliveriesAndMatchesData");


const  matchesPlayedForYear= require("./src/server/1-matches-per-year");
const problem1JsonFilePath = "./src/public/output/1-matchesPerYear.json";

const  matchesPlayedForYearResult= matchesPlayedForYear(allMatches);

fs.writeFileSync(
    problem1JsonFilePath,
    JSON.stringify(matchesPlayedForYearResult, null, 2)
);
console.log(`MatchesPerYear.json File saved`);



const  matchesWonPerTeamPerYear= require("./src/server/2-matches-won-per-team-per-year");
const problem2JsonFilePath="./src/public/output/2-totalMatchesWonPerTeamPerYear.json";

const matchesWonPerTeamByYear = matchesWonPerTeamPerYear(allMatches);
fs.writeFileSync(
    problem2JsonFilePath,
    JSON.stringify(matchesWonPerTeamByYear, null, 2)
);
console.log(`totalMatchesWonPerTeamPerYear.json File saved`);


const  extraRunsConcededPerTeam= require("./src/server/3-extra-runs-conceded-per-team-2016.js");
const problem3JsonFilePath = "./src/public/output/3-extraRunsConcededPerTeam.json";
const extraRunsConcededPerTeamResult = extraRunsConcededPerTeam(allDeliveries,allMatches);

fs.writeFileSync(
    problem3JsonFilePath,
    JSON.stringify(extraRunsConcededPerTeamResult, null, 2)
);
console.log(`extraRunsConcededPerTeam.json File saved`);

const  topEconomicalBowler= require("./src/server/4-top-10-economical-bowler.js");
const problem4JsonFilePath = "./src/public/output/4-top10EconomicalBowler.json";
const topEconomicalBowlerResult = topEconomicalBowler(allDeliveries,allMatches);

fs.writeFileSync(
    problem4JsonFilePath,
    JSON.stringify(topEconomicalBowlerResult, null, 2)
);
console.log(`top10EconomicalBowler.json File saved`);


const teamWinTossAndWinMatch= require("./src/server/5-team-won-toss.js");
const problem5JsonFilePath = "./src/public/output/5-teamWonTossAndMatchBoth.json";
const teamWonTossAndWonMatchResult = teamWinTossAndWinMatch(allMatches);

fs.writeFileSync(
    problem5JsonFilePath,
    JSON.stringify(teamWonTossAndWonMatchResult, null, 2)
);
console.log(`teamWonTossAndMatchBoth.json File saved`);


const  mostNumberOfPlayerOfMatches= require("./src/server/6-highest-number-of-player-of-match");
const problem6JsonFilePath ="./src/public/output/6-highestNumberOfPlayerOfMatch.json";
const mostNumberOfPlayerOfMatchesResult= mostNumberOfPlayerOfMatches(allMatches);

fs.writeFileSync(
    problem6JsonFilePath,
    JSON.stringify(mostNumberOfPlayerOfMatchesResult, null, 2)
);
console.log(
    `highestNumberOfPlayerOfMatch.json File saved`
);

const strikeRateOfBatsman= require("./src/server/7-strike-rate-of_batsman.js");
const problem7JsonFilePath ="./src/public/output/7-strikeRateOfBatsman.json";
const batsman='V Kohli';
const strikeRateOfPlayerEachSeason =strikeRateOfBatsman(allDeliveries,allMatches,batsman)
    
fs.writeFileSync(
    problem7JsonFilePath,
    JSON.stringify(strikeRateOfPlayerEachSeason, null, 2)
);
console.log(
    `strikeRateOfBatsman.json File saved`
);


const mostNumberOfTimeBatsmanDissimed = require("./src/server/8-highest-number-times-batsman-dismissed.js");
const problem8JsonFilePath ="./src/public/output/8-highestNumberTimesBatsmanDismissed.json";
const onePlayerDismissedAnotherMaxTime =mostNumberOfTimeBatsmanDissimed(allDeliveries);

fs.writeFileSync(
    problem8JsonFilePath,
    JSON.stringify(onePlayerDismissedAnotherMaxTime, null, 2)
);
console.log(
    `highestNumberTimesBatsmanDismissed.json File saved`
);


const bestBowlerEconomy = require("./src/server/9-bowler-best-economy.js");
const problem9JsonFilePath = "./src/public/output/9-bowlerBestEconomy.json";
const bestBowlerEconomyResult = bestBowlerEconomy(allDeliveries,allMatches);

fs.writeFileSync(
    problem9JsonFilePath,
    JSON.stringify(bestBowlerEconomyResult, null, 2)
);
console.log(`bowlerBestEconomy.json File saved`);


